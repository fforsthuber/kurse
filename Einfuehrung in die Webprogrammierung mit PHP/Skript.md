# Einführung in die Webprogrammierung mit PHP

## Ziel des Kurses

Dieser Kurs soll die Grundlagen der Webprogrammierung vermitteln.  
Die Veranstaltung möglichst praxisnah abgehalten werden.
Schon während der Vorstellung der Konzepte kann man bereits "mit coden".
Programmieren werden wir mit der Sprache PHP. Zum Darstellen unserer Webinhalte verwenden wir HTML und CSS.
Dafür setzen wir uns gleich in den ersten Stunden eine lauffähige Programmierumgebung auf.  
Nach jeder Theorieeinheit üben wir das gelernete gleich zusammen anhand meherer
Beispiele. Wenn ihr motiviert seid auch zwischen den Kurseinheiten eure Programme weiter zu entwickeln
gibt es zum Schluss jeder Stunde noch Aufgaben zur Inspiration.  
Am Ende sollt ihr alle Grundkenntnisse besitzen um eine eigene kleine Hobbyseite
selbstständig bauen und hosten zu können.
Außerdem habt ihr dann einen guten Ausgangspunkt euch in Richtung professioneller Webentwicklung weiter zu bilden.  
Aber zunnächst schauen wir uns Infrastruktur einer typischen Webanwendung an.

## Infrastruktur einer Webanwendung

Unter Infrastruktur verstehen wir in diesem Zusammenhang "alles um unsere Webanwendung herum", 
was notwendig ist um eine diese im Internet verfügbar zu machen.
Man könnte dies mit folgendem Bild vergleichen.
Wenn ich ein Auto habe, brauche ich Straßen, damit ich darauf fahren kann.
Außerdem benötige ich Verkehrszeichen und ein allgemein gültiges Regelwerk,
die Straßenverkehrordnung, damit das Gesamtsystem "Verkehr" miit allen beteiligten funktionieren kann.
Ähnlich verhält es sich mit meiner Webanwendung. Sie lebt in einem bestehendem Netztwerk, dem Internet.
Sie muss gewisse Regeln, sogenannte Protokolle befolgen, damit sie im Netz erreichbar und bedienbar ist.

### Server und Client

Beim Wort Server denken wir zunächst mal an große Regale mit dutzenden Rechnern
darin und Unmengen an Kabeln.

![servers](skript-resources/bilder/servers.png "Server in einem Rack")  

Tatsächlich ist ein Server erstmal lediglich nur ein Programm. In unserem Fall ist dies ein Webserver.  
Die Aufgabe eines solchen Programms ist das Annhemen von Anfragen oder "Requests" und das Senden einer 
Anwort, "Response" an den Anfragenden. Der Webserver leitet die Anfragen an unser PHP Programm weiter. 
Das Ergebnis unseres Programms geht dann wieder zurück an den Webserver und dieser schickt eine Antwort zuück an den Client.
Neben diesen "dynamischen" Anfragen, also Anfragen, wo wir mit unserem PHP Programm eine Anwort "berechnen", kann ein Webserver
auch Anfragen von statischen Inhalten annehmen und diese ausliefern. Unter statischen Inhalten verstehen wir zum Beispiel Bilder oder Videos.

![servers](skript-resources/bilder/webserver.png "Schaubild Webservers")

Das Programm, welches diese Anfragen an einen Server sendet, nennt man Client. Ein Client kann viele Formen haben, aber in unserem
Fall sind es meistens Webbrowser, wie Firefox oder Google Chrome, oder aber auch eine App auf dem Smartphone.

> *In den Browsertools eines Webbrowsers kann man die verschiedenen Anfragen beim Aufruf einer Seite erkennen*  

![servers](skript-resources/bilder/requests.png "Anfragen am Beispiel Webbrowser")

#### Wie der Client den Server findet

Jeder Teilnehmer am Internet, also jeder Client und jeder Server, besitzt eine sogenannte IP Adresse.
Diese kann man in etwa einer Adresse in einem Ort oder einer Stadt vergleichen. 
Mit der Angabe von Strasse, Nummer, PLZ, Ort und Land ist sie weltweit eindeutig und identifiziert das
dort befindliche Gebäude.
Clients und Server werden auf ähnliche Weise mit der maximal 12 stelligen IP Adresse identifiziert und können so über 
zentrale Knotenpunkte im Internet miteinander kommunizieren.
Der Aufbau einer IP Adresse ist folgender:  
```
[0 - 255].[0 - 255].[0 - 255].[0 - 255]

Beispiele:
- 81.89.255.130
- 127.0.0.1
- 8.0.0.0
```
> *Die 12 maximal stellige Variante der IP Adresse ist Version 4 und eigentlich schon veraltet, wird aber noch weitläufig eingestezt.*  
> *Der neue Standard ist Version 6 mit sehr viel mehr möglichen Adressen.*

Da wir Menschen uns allerdings schwer tun uns für jede Website eine mehrstellige Zahl zu merken, gibt es einen sogeannten DNS
(Domain Name Service). Dieser Service erlaubt es für Server Namen zu vergeben. Tippen wir dann z.B. im Browser einen Namen wie *php.de*
in die Adressleiste ein, fragt dieser beim DNS nach der zughörigen IP Adresse. Wenn der Browser dann die IP Adresse
kennt, kann er eine Anfrage an den refrenzierten Server schicken.

![servers](skript-resources/bilder/dns.png "Schaubild DNS")

#### Wie der Client mit dem Server spricht

Wenn sich Client und Server gefunden haben, kann die Kommunikation zwischen beiden beginnen.
In welcher Formm diese Kommunikation statt finden muss, damit sich beide Parteien verstehen,
regelt das HTTP (HyperTextTransferProtokoll).
Darin ist unter andrem geregelt, wie eine Anfrage vom Client aufgebaut werden muss, aber auch in welchem
Format die Antwort eines Servers zu erwarten ist.
Eine Anfrage kann zum Beispiel verschiedene Parameter beinhalten und auf unterschiedliche Art übertragen werden. 
Eine Anwort kann unterschiedliche Status Codes haben und muss nicht immer Daten beinhalten.

> *Eine Antwort beinhaltet zumindest immer Metadaten, sogenannte Header und einen Statuscode*

HTTP Nachrichten sind nicht verschlüsselt, d.h. Inhalte werden im Klartext übertragen. Da im Internet auch sensible
Anwendungen wie z.B. Online Banking oder Zahlungen in einem Webshop statt finden, gibt es HTTPS.
Diese Protokoll Erweiterung kümmert sich um die Verschlüsselung von Nachrichten.

```php
<?php
    declare(strict_types=1);
    
    $test = 'test';
    
    function test(string $arg1, bool $arg2): string
    {
    
    }
```

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:69d9497218281124c307ebaf4eb2498c?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:69d9497218281124c307ebaf4eb2498c?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:69d9497218281124c307ebaf4eb2498c?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/fforsthuber/kurse.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/-/experiment/new_project_readme_content:69d9497218281124c307ebaf4eb2498c?https://docs.gitlab.com/ee/user/project/integrations/)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://gitlab.com/-/experiment/new_project_readme_content:69d9497218281124c307ebaf4eb2498c?https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://gitlab.com/-/experiment/new_project_readme_content:69d9497218281124c307ebaf4eb2498c?https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://gitlab.com/-/experiment/new_project_readme_content:69d9497218281124c307ebaf4eb2498c?https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Automatically merge when pipeline succeeds](https://gitlab.com/-/experiment/new_project_readme_content:69d9497218281124c307ebaf4eb2498c?https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://gitlab.com/-/experiment/new_project_readme_content:69d9497218281124c307ebaf4eb2498c?https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://gitlab.com/-/experiment/new_project_readme_content:69d9497218281124c307ebaf4eb2498c?https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://gitlab.com/-/experiment/new_project_readme_content:69d9497218281124c307ebaf4eb2498c?https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://gitlab.com/-/experiment/new_project_readme_content:69d9497218281124c307ebaf4eb2498c?https://docs.gitlab.com/ee/user/clusters/agent/)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!).  Thank you to [makeareadme.com](https://gitlab.com/-/experiment/new_project_readme_content:69d9497218281124c307ebaf4eb2498c?https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

